const express = require('express')
const sls = require('serverless-http')
const bodyParser = require('body-parser');
const cors = require('cors');
const config = require('./config/config');
const app = express()

app.use(cors());
app.use(bodyParser.json());
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'Content-Type');
    next();
});

app.get('/health', (req, res) => {
    res.send({
        statusCode: 200,
        message: "I'm Good, very good..."
    })
});

/**
 * route handlers
 */
const getVideosInfo = require('./routes/GetVideosInfo');
const getVideos = require('./routes/GetVideos');

app.use(config.version, getVideosInfo);
app.use(config.version, getVideos);

if (process.env.ENVIRONMENT && process.env.ENVIRONMENT === "LOCAL") {
    app.listen(config.PORT, () => console.log(`running on port ${config.PORT}`));
} else {
    console.log("start serverless app");
    module.exports.server = sls(app);
}