const express = require('express');
const router = express.Router();
var AWS = require('aws-sdk');
const config = require('../config/config');

router.post('/getVideos', (req, res) => {
    console.log('enter get videos API')
    //let limit = req.body.limit;

    AWS.config.update({region: config.DynamoDB.Regions});

    var docClient = new AWS.DynamoDB.DocumentClient();

    var params = {
        TableName: config.DynamoDB.TableName,
        Key: { 
            idx: 0,
        },
    }

    docClient.get(params, function(err, data) {
        if (err) {
            res.send(err);
        } else {
            res.send(data);
        }
    });
});

module.exports = router;
