const express = require('express');
const router = express.Router();
const request = require('request-promise');
const config = require('../config/config');

router.post('/getVideosInfo', (req, res) => {
    console.log('enter get videos info API')
    let videos = req.body.videos;
    fetch_videos_info(req, res, videos);
});

fetch_videos_info = (req, res, videos)=> {
    let promiseArray = [];
    videos.forEach((id)=>{
        promiseArray.push(Promise.resolve(fetch_video_info(id)));
    });

    Promise.all(promiseArray).then((result)=>{
        res.send(result);
    })
}

fetch_video_info = (id) => {
    return new Promise((resolve, reject)=>{
        var options = {
            method: 'GET',
            uri: config.Youtube_API.videosUrl + id + '&key='
             + config.Youtube_API.ApiKey,
            headers: {
                'Content-Type': 'application/json',
            },
            json: true
        };
    
        request(options).then((response)=>{
            resolve({
                id : response.items[0].id,
                title : response.items[0].snippet.title,
                thumbnails : response.items[0].snippet.thumbnails.medium
            });
        }) 
    })
    .catch((err)=>{
        reject('internal server error')
    })   
}

module.exports = router;
